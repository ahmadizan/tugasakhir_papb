package id.ac.ub.fbauth;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class AddActivity extends AppCompatActivity {
    EditText etTitleAdd, etNoteAdd;
    Button bt_back, btAddNote;
    DatabaseReference database = FirebaseDatabase.getInstance().getReference();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_note);
        etTitleAdd = findViewById(R.id.etTitleAdd);
        etNoteAdd = findViewById(R.id.etNoteAdd);
        btAddNote = findViewById(R.id.btAddNote);
        bt_back = findViewById(R.id.bt_backAdd);

        bt_back.setOnClickListener(v ->{
            startActivity(new Intent(getApplicationContext(), Homepage.class));
        });

        btAddNote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String getTitle = etTitleAdd.getText().toString();
                String getBody = etNoteAdd.getText().toString();

                if(getTitle.isEmpty()){
                    Toast.makeText(getApplicationContext(), "Please Add the Title Before Add the new Notes", Toast.LENGTH_SHORT).show();
                }else if(getBody.isEmpty()){
                    Toast.makeText(getApplicationContext(),"Please fill the Notes before Add the new Notes", Toast.LENGTH_SHORT).show();
                }else{
                    database.child("Notes").push().setValue(new ModelNotes(getTitle,getBody)).addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void unused) {
                            Toast.makeText(getApplicationContext(), "Notes Berhasil Dibuat", Toast.LENGTH_SHORT).show();
                            startActivity(new Intent(getApplicationContext(),Homepage.class));
                            finish();
                        }
                    }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Toast.makeText(getApplicationContext(), "Notes Gagal Disimpan", Toast.LENGTH_SHORT).show();

                        }
                    });
                }
            }
        });
    }
}