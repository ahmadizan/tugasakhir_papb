package id.ac.ub.fbauth;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class  EditActivity extends AppCompatActivity{
    Button btBackEdit, btUpdateNote, btDeleteNote;
    EditText etTitleEdit, etNoteEdit;
    String key,title,note;
    DatabaseReference database = FirebaseDatabase.getInstance().getReference();

//    public EditActivity(String title, String note) {
//        this.title = title;
//        this.note = note;
//    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_note);
        btBackEdit = findViewById(R.id.bt_backEdit);
        btUpdateNote = findViewById(R.id.btUpdateNote);
        etTitleEdit = findViewById(R.id.etTitleEdit);
        etNoteEdit = findViewById(R.id.etNoteEdit);

        btBackEdit.setOnClickListener(view -> {
            startActivity(new Intent(getApplicationContext(),Homepage.class));
        });
        Intent intent = getIntent();
        String title = intent.getStringExtra("DataTitle");
        String note = intent.getStringExtra("DataNote");

        etTitleEdit.setText(title);
        etNoteEdit.setText(note);

        btUpdateNote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String title = etTitleEdit.getText().toString();
                String note = etNoteEdit.getText().toString();
                database.child("Notes").child(key).setValue(new ModelNotes(title,note)).addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void unused) {
                        Toast.makeText(view.getContext(),"Upate Success",Toast.LENGTH_SHORT).show();
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(view.getContext(),"Upate Failed",Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });

    }
}
