package id.ac.ub.fbauth;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

public class LandingActivity extends AppCompatActivity{
    Button bt_start;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.landing_page);

        bt_start = findViewById(R.id.btStart);
        bt_start.setOnClickListener(v ->{
            startActivity(new Intent(getApplicationContext(), LoginActivity.class));
        });
    }
}
