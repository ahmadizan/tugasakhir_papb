package id.ac.ub.fbauth;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class Homepage extends AppCompatActivity{
    Button bt1;
    TextView tv2;
    FirebaseUser firebaseUser;
    String Username;
    AdapterNotes adapterNotes;
    RecyclerView rvNotes;
    ArrayList<ModelNotes> listNotes;
    DatabaseReference database = FirebaseDatabase.getInstance().getReference();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.homepage);
        bt1 = findViewById(R.id.bt1);
        tv2 = findViewById(R.id.tv2);

        firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        Username = firebaseUser.getDisplayName();
        tv2.setText("Hello, "+Username);

        bt1.setOnClickListener(v ->{
            startActivity(new Intent(getApplicationContext(), AddActivity.class));
        });

        tv2.setOnClickListener(v ->{
            startActivity(new Intent(getApplicationContext(), ProfileActivity.class));
        });
        rvNotes = findViewById(R.id.rvNotes);
        RecyclerView.LayoutManager myLayout = new LinearLayoutManager(this);
        rvNotes.setLayoutManager(myLayout);
        rvNotes.setItemAnimator(new DefaultItemAnimator());

        tampilData();
    }

    private void tampilData() {
        database.child("Notes").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                listNotes = new ArrayList<>();
                for(DataSnapshot item : snapshot.getChildren()){
                    ModelNotes notes = item.getValue(ModelNotes.class);
                    notes.setKey(item.getKey());
                    listNotes.add(notes);
                }
                adapterNotes = new AdapterNotes(listNotes, Homepage.this);
                rvNotes.setAdapter(adapterNotes);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }
}
