package id.ac.ub.fbauth;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class ProfileActivity extends AppCompatActivity{
    Button bt_logout, bt_back;
    TextView tvName, tvEmail;
    private FirebaseUser firebaseUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profile);
        tvEmail = findViewById(R.id.tvEmail2);
        tvName = findViewById(R.id.tvName2);
        bt_logout = findViewById(R.id.btLogout);
        bt_back = findViewById(R.id.bt_backProfil);

        firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        tvEmail.setText(firebaseUser.getEmail());
        tvName.setText(firebaseUser.getDisplayName());

        bt_logout.setOnClickListener(v ->{
            FirebaseAuth.getInstance().signOut();
            startActivity(new Intent(getApplicationContext(), LandingActivity.class));
            finish();
        });

        bt_back.setOnClickListener(v ->{
            startActivity(new Intent(getApplicationContext(), Homepage.class));
        });
    }
}
