package id.ac.ub.fbauth;

import android.os.Parcel;
import android.os.Parcelable;

public class ModelNotes implements Parcelable {
    private String Title;
    private String Body;
    private String Key;

    public ModelNotes(){

    }

    public ModelNotes(String Title, String Body){
        this.Body = Body;
        this.Title = Title;
    }

    protected ModelNotes(Parcel in) {
        Title = in.readString();
        Body = in.readString();
        Key = in.readString();
    }

    public static final Creator<ModelNotes> CREATOR = new Creator<ModelNotes>() {
        @Override
        public ModelNotes createFromParcel(Parcel in) {
            return new ModelNotes(in);
        }

        @Override
        public ModelNotes[] newArray(int size) {
            return new ModelNotes[size];
        }
    };

    public String getKey() {
        return Key;
    }

    public void setKey(String key) {
        Key = key;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getBody() {
        return Body;
    }

    public void setBody(String body) {
        Body = body;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(Title);
        parcel.writeString(Body);
        parcel.writeString(Key);
    }
}
