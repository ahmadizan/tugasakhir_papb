package id.ac.ub.fbauth;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.List;

public class AdapterNotes extends RecyclerView.Adapter<AdapterNotes.MyViewHolder> {
    private List<ModelNotes> mNotes;
    private Activity activity;
    DatabaseReference database = FirebaseDatabase.getInstance().getReference();

    public AdapterNotes(List<ModelNotes> mNotes, Activity activity){
        this.mNotes = mNotes;
        this.activity = activity;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View viewItem = inflater.inflate(R.layout.row,parent,false);
        return new MyViewHolder(viewItem);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        final ModelNotes data = mNotes.get(position);
        holder.tvJudulNotes.setText(data.getTitle());
        holder.tvBodyNotes.setText(data.getBody());

        holder.cardNotes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(activity,DetailNote.class);
                intent.putExtra("DataTitle", data.getTitle());
                intent.putExtra("DataNote", data.getBody());
                intent.putExtra("DataKey", data.getKey());
                activity.startActivity(intent);
            }
        });
        holder.cardNotes.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                FragmentManager manager = ((AppCompatActivity)activity).getSupportFragmentManager();
                DialogForn dialogForn = new DialogForn(
                  data.getTitle(),
                  data.getBody(),
                        data.getKey(),
                        "Ubah"

                );
                dialogForn.show(manager , "form");
                return true;
            }
        });
//        holder.btEditNote.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent intent = new Intent(activity,EditActivity.class);
//                activity.startActivity(intent);
//            }
//        });
    }

    @Override
    public int getItemCount() {
        return mNotes.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tvJudulNotes,tvBodyNotes;
        CardView cardNotes;
        Button btEditNote;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            tvJudulNotes = itemView.findViewById(R.id.tvJudulNotes);
            tvBodyNotes = itemView.findViewById(R.id.tvBodyNotes);
            cardNotes = itemView.findViewById(R.id.cardNotes);
            btEditNote = itemView.findViewById(R.id.btUpdateNote);
        }
    }
}
