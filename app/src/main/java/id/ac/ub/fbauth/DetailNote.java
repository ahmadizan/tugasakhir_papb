package id.ac.ub.fbauth;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class DetailNote extends AppCompatActivity {
    TextView tvTitle, tvNote;
    Button btEditNote, btBack1;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.detail_note);
        tvTitle = findViewById(R.id.tvTitle);
        tvNote = findViewById(R.id.tvNote);
        btEditNote = findViewById(R.id.btUpdateNote);
        btBack1 = findViewById(R.id.bt_backDetail12);

        btBack1.setOnClickListener(view -> {
            startActivity(new Intent(getApplicationContext(), Homepage.class));
        });
        Intent intent = getIntent();
        String title = intent.getStringExtra("DataTitle");
        String note = intent.getStringExtra("DataNote");
        String Key = intent.getStringExtra("DataKey");

        tvTitle.setText(title);
        tvNote.setText(note);

//        FirebaseDatabase mydata = FirebaseDatabase.getInstance();
//        DatabaseReference myref = mydata.getReference().child("Notes");
//        myref.orderByChild("DataTitle").equalTo(title).addListenerForSingleValueEvent(new ValueEventListener() {
//            @Override
//            public void onDataChange(@NonNull DataSnapshot snapshot) {
//                for (DataSnapshot snapshot1 : snapshot.getChildren()){
//                    String snap = snapshot1.getKey();
//
//                    btEditNote.setOnClickListener(view -> {
//                        if (Key.equals(snap)){
//                            DatabaseReference mydataref = mydata.getReference().child("DataTitle");
//                            mydataref.orderByChild("DataTitle").equalTo(title).addListenerForSingleValueEvent(new ValueEventListener() {
//                                @Override
//                                public void onDataChange(@NonNull DataSnapshot snapshot) {
//                                    for (DataSnapshot snapshot1 : snapshot.getChildren()){
//                                        String snap2 = snapshot1 .getKey();
//                                        Intent intent = new Intent(DetailNote.this, EditActivity.class);
//                                        intent.putExtra("DataTitle", title);
//                                        intent.putExtra("DataNote", note);
//                                        intent.putExtra("DataKey", snap2);
//                                        startActivity(intent);
//                                    }
//                                }
//
//                                @Override
//                                public void onCancelled(@NonNull DatabaseError error) {
//
//                                }
//                            });
//                        }
//                    });
//                }
//            }
//
//            @Override
//            public void onCancelled(@NonNull DatabaseError error) {
//
//            }
//        });

        btEditNote.setOnClickListener(view -> {
            startActivity(new Intent(getApplicationContext(),EditActivity.class));
        });
    }
}
